package com.freecharge.merchant.utils;

import android.support.annotation.Nullable;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

public class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode> {
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private BarcodeGraphicTracker.BarcodeAutoDetectionListener barcodeAutoDetectionListener;

    public BarcodeTrackerFactory(GraphicOverlay<BarcodeGraphic> barcodeGraphicOverlay) {
        mGraphicOverlay = barcodeGraphicOverlay;
    }

    public BarcodeTrackerFactory(GraphicOverlay<BarcodeGraphic> barcodeGraphicOverlay,
                                 @Nullable BarcodeGraphicTracker.BarcodeAutoDetectionListener barcodeAutoDetectionListener) {
        mGraphicOverlay = barcodeGraphicOverlay;
        this.barcodeAutoDetectionListener = barcodeAutoDetectionListener;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode) {
        BarcodeGraphic graphic = new BarcodeGraphic(mGraphicOverlay);
        BarcodeGraphicTracker tracker = new BarcodeGraphicTracker(mGraphicOverlay, graphic);
        if (barcodeAutoDetectionListener != null) {
            tracker.setNewDetectionListener(barcodeAutoDetectionListener);
        }
        return tracker;
    }
}
