package com.freecharge.merchant.utils;

public interface OnCameraPreviewErrorListener {

    void onCameraPreviewErrorOccured(Exception e);

}
