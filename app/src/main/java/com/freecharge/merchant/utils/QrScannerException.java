package com.freecharge.merchant.utils;

public class QrScannerException extends Exception {
    public static final int ERROR_CAMERA_INIT = 124235245;
    public static final int ERROR_CAMERA_PERMISSION_DENIED = 124235246;
    public static final int ERROR_GALLERY_WRONG_IMAGE = 124235247;
    public int errorCode;

    public QrScannerException(int errorCode) {
        this.errorCode = errorCode;
    }
}
