package com.freecharge.merchant

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.freecharge.merchant.utils.SquareImageView
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class QrCodeCaptureActivity : AppCompatActivity() {

    private val WHITE = -0x1
    private val BLACK = -0x1000000
    private var WIDTH: Int = 400
    private var HEIGHT: Int = 400
    var qrString: String? = null
    var qrCodeIv: SquareImageView? = null
    var shareQr: TextView? = null
    var downloadQr: TextView? = null
    var llQr: LinearLayout? = null
    var llShare: LinearLayout? = null
    private var showingShareScreen = false

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        var b: Bundle? = null
        if (intent != null && intent.extras != null) {
            b = intent.extras
            if (b!!.getString("qrString", "").isNotEmpty()) {
                qrString = b.getString("qrString", "")
            } else {
                finish()
            }
        }

        setContentView(R.layout.layout_qr_preview)
        supportActionBar?.title = "Freecharge QR Code"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.show()
        qrCodeIv = findViewById<SquareImageView>(R.id.qr_code_iv)
        shareQr = findViewById<TextView>(R.id.share_qr)
        downloadQr = findViewById<TextView>(R.id.download_qr)
        llQr = findViewById<LinearLayout>(R.id.ll_qr)
        llShare = findViewById<LinearLayout>(R.id.ll_share)

        setQrImage()

        downloadQr?.setOnClickListener(View.OnClickListener {
            downloadQrCode(false)
        })

        shareQr?.setOnClickListener(View.OnClickListener {
            downloadQrCode(true)
        })


    }

    private fun setQrImage() {
        try {
            qrCodeIv?.setImageBitmap(encodeAsBitmap(qrString!!))
        } catch (e: WriterException) {
            e.printStackTrace()
        }

    }


    @Throws(WriterException::class)
    private fun encodeAsBitmap(str: String): Bitmap? {
        val result: BitMatrix
        val hints: MutableMap<EncodeHintType, Any>

        hints = HashMap()
        hints[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.H
        try {
            result = MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, WIDTH, HEIGHT, hints)
        } catch (iae: Exception) {
            // Unsupported format
            iae.printStackTrace()
            return null
        }

        val width = result.width
        val height = result.height
        val pixels = IntArray(width * height)
        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (result.get(x, y)) BLACK else WHITE
            }
        }

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap

    }

    fun downloadQrCode(isShare: Boolean) {
        store(
            getScreenShot(llQr!!),
            SimpleDateFormat("yyyyMMddhhmmss'_qr.jpg'").format(Date()),
            isShare
        )
    }

    private fun getScreenShot(view: View): Bitmap {
        view.isDrawingCacheEnabled = true
        val bitmap = Bitmap.createBitmap(view.drawingCache)
        view.isDrawingCacheEnabled = false
        return bitmap
    }

    private fun store(bm: Bitmap, fileName: String, isShare: Boolean) {

        // Fix for
        // file:// is not allowed to attach with Intent anymore or it will throw FileUriExposedException which may cause your app crash immediately called
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

//        val dirPath =
//            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).absolutePath + "/Downloads"
        val dirPath =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath
        val dir = File(dirPath)
        if (!dir.exists())
            dir.mkdirs()
        val file = File(dirPath, fileName)
        try {
            val fOut = FileOutputStream(file)
            bm.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            fOut.flush()
            fOut.close()
            if (!isShare)
                Toast.makeText(this, "QR Code Downloaded", Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        MediaScannerConnection.scanFile(
            this@QrCodeCaptureActivity, arrayOf(file.path), null
        ) { path, uri -> }

        if (isShare && !showingShareScreen) {
            val uri = Uri.fromFile(file)
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.type = "image/*"

            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "")
            intent.putExtra(android.content.Intent.EXTRA_TEXT, "")
            intent.putExtra(Intent.EXTRA_STREAM, uri)
            try {
                startActivity(Intent.createChooser(intent, "Share QR Code"))
                showingShareScreen = true
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(this, "No App Available", Toast.LENGTH_LONG).show()
            }

        }
    }

    override fun onResume() {
        super.onResume()
        showingShareScreen = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun mmToPx(mm: Int): Int {
        val px = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_MM, mm.toFloat(),
            resources.displayMetrics
        )

        return px.toInt()
    }
}