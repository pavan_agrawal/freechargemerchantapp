package com.freecharge.merchant

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class TransparentActivity : AppCompatActivity() {

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)

        var b: Bundle? = null
        if (intent != null && intent.extras != null) {
            b = intent.extras
            if (b!!.getBoolean("isKill", false)) {
                setResult(RESULT_CANCELED)
                finish()
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        if (intent != null) {
            val b = intent.extras
            if (b != null) {
                if (b.getBoolean("isKill", false)) {
                    setResult(RESULT_CANCELED)
                    finish()
                }
            }
        }
    }

}