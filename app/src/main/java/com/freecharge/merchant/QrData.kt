package com.freecharge.merchant
import com.google.gson.annotations.SerializedName

data class QrData(@SerializedName("qr") val qr: String? = null)