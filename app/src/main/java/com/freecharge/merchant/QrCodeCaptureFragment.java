package com.freecharge.merchant;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.freecharge.merchant.utils.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.*;
import com.google.zxing.common.HybridBinarizer;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

@RuntimePermissions
public class QrCodeCaptureFragment extends Fragment implements View.OnClickListener, OnCameraPreviewErrorListener {
    // constants used to pass extra data in the intent
    public static final String AutoFocus = "AutoFocus";
    public static final String UseFlash = "UseFlash";
    public static final String AutoDetect = "autodetect";
    private static final String TAG = "Barcode-reader";
    // intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;
    private static final int SELECT_PHOTO = 101;
    private static List<QrCodeResultReceiver> mReceivers = new ArrayList<>();
    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private View mTopLayout;
    private TextView galleryTv;
    private EditText qRString;
    private ImageView done;
    static MainActivity mainActivity;
    private boolean isDetecting = true;
    private boolean isNeverAskAgain = false;

    public static QrCodeCaptureFragment newInstance(MainActivity context, boolean autoFocus, boolean useFlash,
                                                    boolean autoDetect,
                                                    @NonNull QrCodeResultReceiver receiver) {
        mainActivity = context;
        Bundle args = new Bundle();
        args.putBoolean(AutoFocus, autoFocus);
        args.putBoolean(UseFlash, useFlash);
        args.putBoolean(AutoDetect, autoDetect);
        QrCodeCaptureFragment fragment = new QrCodeCaptureFragment();
        fragment.setArguments(args);
        addQrCodeReceiver(receiver);
        return fragment;
    }


    private static void addQrCodeReceiver(QrCodeResultReceiver receiver) {

        for (QrCodeResultReceiver qrCodeResultReceiver : mReceivers) {
            if (qrCodeResultReceiver.getClass().getSimpleName().equals(receiver.getClass().getSimpleName())) {
                mReceivers.remove(qrCodeResultReceiver);
            }
        }
        mReceivers.add(receiver);
    }

    public static String scanQRImage(Bitmap bMap) {
        String contents = null;

        int[] intArray = new int[bMap.getWidth() * bMap.getHeight()];
        //copy pixel data from the Bitmap into the 'intArray' array
        bMap.getPixels(intArray, 0, bMap.getWidth(), 0, 0, bMap.getWidth(), bMap.getHeight());

        LuminanceSource source = new RGBLuminanceSource(bMap.getWidth(), bMap.getHeight(), intArray);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Reader reader = new MultiFormatReader();
        try {
            Result result = reader.decode(bitmap);
            contents = result.getText();
        } catch (Exception e) {
            Log.e("QrTest", "Error decoding barcode", e);
        }
        return contents;
    }

    /**
     * Initializes the UI and creates the detector pipeline.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.barcode_capture, container, false);

        mPreview = (CameraSourcePreview) rootView.findViewById(R.id.preview);
        mPreview.setOnCameraErrorListener(this);
        mGraphicOverlay = (GraphicOverlay<BarcodeGraphic>) rootView.findViewById(R.id.graphicOverlay);
        mTopLayout = rootView.findViewById(R.id.topLayout);
        Bundle bundle = getArguments();

        galleryTv = (TextView) rootView.findViewById(R.id.capture_gallery);

        galleryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPic = new Intent(Intent.ACTION_PICK);
                photoPic.setType("image/*");
                startActivityForResult(photoPic, SELECT_PHOTO);
            }
        });


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            setVisiblityNoPermissionLayout(true);
            if (getUserVisibleHint())
                com.freecharge.merchant.QrCodeCaptureFragmentPermissionsDispatcher.openCameraWithCheck(this);
        } else {
            setVisiblityNoPermissionLayout(false);
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getView() != null && isAdded()) {
            com.freecharge.merchant.QrCodeCaptureFragmentPermissionsDispatcher.openCameraWithCheck(this);
        }
    }

    public void startReceivingDetections() {
        isDetecting = true;
    }


    @NeedsPermission(Manifest.permission.CAMERA)
    void openCamera() {
        Bundle bundle = getArguments();
        setVisiblityNoPermissionLayout(false);
        if (bundle != null) {
            isDetecting = true;
            boolean autoFocus = bundle.getBoolean(AutoFocus, false);
            boolean useFlash = bundle.getBoolean(UseFlash, false);
            boolean autoDetect = bundle.getBoolean(AutoDetect, false);
            createCameraSource(autoFocus, useFlash, autoDetect);
        }

    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    void onPermissionNeverAskAgain() {
        setVisiblityNoPermissionLayout(true);
        isNeverAskAgain = true;
    }


    @OnPermissionDenied(Manifest.permission.CAMERA)
    void onCameraPermissionDenied() {
        setVisiblityNoPermissionLayout(true);
    }


    private void setVisiblityNoPermissionLayout(boolean visible) {
        if (getView() != null) {
            if (visible) {
                getView().findViewById(R.id.layout_noPermission).setVisibility(View.VISIBLE);
                mTopLayout.setVisibility(View.GONE);
                getView().findViewById(R.id.lt_alternateOptions).setOnClickListener(this);
                getView().findViewById(R.id.btn_actionSettings).setOnClickListener(this);
            } else {
                getView().findViewById(R.id.layout_noPermission).setVisibility(View.GONE);
                mTopLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     * <p>
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash, boolean autoDetect) {
        Activity act = getActivity();
        Context context = act != null ? act.getApplicationContext() : null;
        if (context == null)
            return;
        // A barcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the barcode detection results, track the barcodes, and maintain
        // graphics for each barcode on screen.  The factory is used by the multi-processor to
        // create a separate tracker instance for each barcode.

        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context)
                // removing this constraint improves the scanning speed for QR codes.
//                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        BarcodeTrackerFactory barcodeFactory;
        if (!autoDetect) {
            barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay);
        } else {
            //autodetect is enabled, automatic detect barcode
            barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay,
                    new BarcodeGraphicTracker.BarcodeAutoDetectionListener() {
                        @Override
                        public void onNewBarcodeDetection(Barcode barcode) {
                            if (barcode != null && isDetecting) {
//                                LoggerUtils.logDebug("BarcodeDetected", barcode.displayValue);
                                isDetecting = false;
                                onCodeCaptured(barcode.rawValue);
                            }
                        }
                    });
        }

        MultiProcessor<Barcode> mMultiProcessor = new MultiProcessor.Builder<>(barcodeFactory).build();
        barcodeDetector.setProcessor(mMultiProcessor);

        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the barcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
//            LoggerUtils.logWarn(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = context.registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(context, R.string.low_storage_error, Toast.LENGTH_LONG).show();
//                LoggerUtils.logWarn(TAG, getString(R.string.low_storage_error));
            }
        }

        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the barcode detector to detect small barcodes
        // at long distances.
        CameraSource.Builder builder = new CameraSource.Builder(context, barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);

        // make sure that auto focus is an available option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        mCameraSource = builder
                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                .build();
    }

    /**
     * Restarts the camera.
     */
    @Override
    public void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {

            setVisiblityNoPermissionLayout(false);

            if (mCameraSource == null) {
                openCamera();
            }
            startCameraSource();
        }
    }

    /**
     * Stops the camera.
     */
    @Override
    public void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            try {
                mPreview.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        com.freecharge.merchant.QrCodeCaptureFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);

        if (requestCode != RC_HANDLE_CAMERA_PERM) {
//            LoggerUtils.logDebug(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }
    }


    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        Activity activity = getActivity();
        if (activity == null || activity.getApplicationContext() == null)
            return;
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                activity.getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(activity, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
//                FileLogger.debug("CAMERA", "Starting camera at " + this.getClass().getSimpleName());
                mPreview.start(mCameraSource, mGraphicOverlay);
//                FileLogger.debug("CAMERA", "Camera started at " + this.getClass().getSimpleName());

            } catch (IOException e) {
//                FileLogger.debug("CAMERA", "IOException catched" + e.getMessage());
                showToast(getString(R.string.error_camera_init_failed));

                if (mCameraSource != null)
                    mCameraSource.release();

                mCameraSource = null;

//                LoggerUtils.logErr(TAG, "Unable to start camera source.", e);
                onError(new QrScannerException(QrScannerException.ERROR_CAMERA_INIT));
                finish();
            } catch (SecurityException e) {
//                FileLogger.debug("CAMERA", "SecurityException catched" + e.getMessage());

                isNeverAskAgain = true;
                if (mCameraSource != null)
                    mCameraSource.release();
                setVisiblityNoPermissionLayout(true);

            } catch (RuntimeException e) {
                //security exception does'nt get called below ANDROID M
//                FileLogger.debug("CAMERA", "RuntimeException catched" + e.getMessage());
                isNeverAskAgain = true;
                if (mCameraSource != null)
                    mCameraSource.release();
                setVisiblityNoPermissionLayout(true);
            }
        }
    }

    public void removeReceiver(QrCodeResultReceiver resultReceiver) {
        mReceivers.remove(resultReceiver);
    }

    private void onCodeCaptured(final String barcode) {
        if (mainActivity != null) {
            mainActivity.runOnUiThread(new CallbackRunnable(barcode, this));
        }
    }

    private void onError(QrScannerException t) {
        if (mainActivity != null)
            mainActivity.runOnUiThread(new CallbackRunnable(t));
    }

    private void finish() {
        if (mainActivity != null)
            for (QrCodeResultReceiver receiver : mReceivers) {
                receiver.removeFragment(this);
            }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    InputStream imageStream = null;
                    try {
                        //getting the image
                        imageStream = mainActivity.getContentResolver().openInputStream(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                    String decoded = scanQRImage(bitmap);
                    Log.i("QrTest", "Decoded string=" + decoded);
                    if (!TextUtils.isEmpty(decoded))
                        onCodeCaptured(decoded);
                    else
                        onError(new QrScannerException(QrScannerException.ERROR_GALLERY_WRONG_IMAGE));
                }
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_actionSettings:
                if (isNeverAskAgain)
                    openAppSettings(mainActivity);
                else
                    com.freecharge.merchant.QrCodeCaptureFragmentPermissionsDispatcher.openCameraWithCheck(this);
                break;

            case R.id.lt_alternateOptions:
                galleryTv.performClick();
                break;

        }

    }

    @Override
    public void onCameraPreviewErrorOccured(Exception e) {

        if (e != null && (e instanceof SecurityException || e instanceof RuntimeException)) {
            isNeverAskAgain = true;
            setVisiblityNoPermissionLayout(true);
        } else if (e != null && e instanceof IOException) {
            showToast(mainActivity.getString(R.string.error_camera_init_failed));

            if (mCameraSource != null)
                mCameraSource.release();

            mCameraSource = null;

//            LoggerUtils.logErr(TAG, "Unable to start camera source.", e);
            onError(new QrScannerException(QrScannerException.ERROR_CAMERA_INIT));
            finish();
        }
    }

    public interface QrCodeResultReceiver {
        void onCodeCaptured(String barcode, QrCodeCaptureFragment fragment);

        void onError(QrScannerException t);

        void removeFragment(QrCodeCaptureFragment thisFragment);
    }

    /**
     * This runnable is used to perform the callbacks on UI thread.
     */
    private static class CallbackRunnable implements Runnable {
        private String mBarcodeToPassAlong;
        private QrScannerException mExceptionToPassAlong;
        private WeakReference<QrCodeCaptureFragment> fragmentWeakReference;

        private CallbackRunnable(QrCodeCaptureFragment fragment) {
            fragmentWeakReference = new WeakReference<>(fragment);
        }

        private CallbackRunnable(QrScannerException ex) {
            mExceptionToPassAlong = ex;
        }

        private CallbackRunnable(String barcode, QrCodeCaptureFragment fragment) {
            mBarcodeToPassAlong = barcode;
            fragmentWeakReference = new WeakReference<>(fragment);
        }

        @Override
        public void run() {
            if (mBarcodeToPassAlong != null && fragmentWeakReference != null) {
                QrCodeCaptureFragment fragment = fragmentWeakReference.get();
                if (fragment != null) {
                    for (QrCodeResultReceiver receiver : mReceivers) {
                        receiver.onCodeCaptured(mBarcodeToPassAlong, fragment);
                    }
                }
            } else if (mExceptionToPassAlong != null) {
                for (QrCodeResultReceiver receiver : mReceivers) {
                    receiver.onError(mExceptionToPassAlong);
                }
            }
        }
    }

    public void showToast(String message) {
        Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String message, int gravity) {
        Toast toast = Toast.makeText(mainActivity, message, Toast.LENGTH_SHORT);
        toast.setGravity(gravity, 0, 0);
        toast.show();
    }

    public static void openAppSettings(MainActivity activity) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setData(Uri.parse("package:" + activity.getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        activity.startActivity(intent);
    }

}
