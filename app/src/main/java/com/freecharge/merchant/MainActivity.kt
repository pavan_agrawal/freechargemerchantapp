package com.freecharge.merchant

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.DownloadManager
import android.content.ContentValues
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.support.annotation.NonNull
import android.support.annotation.Nullable
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.RelativeLayout
import android.widget.Toast
import com.freecharge.merchant.utils.PathUtils
import com.freecharge.merchant.utils.QrScannerException
import com.google.gson.Gson
import com.google.zxing.WriterException
import id.zelory.compressor.Compressor
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), QrCodeCaptureFragment.QrCodeResultReceiver {

    val FILE_CHOOSER = 101
    val IMAGE_CAPTURE = 102
    val PERMISSION_DENIED = 103
    val SCAN_QR = 104
    var mUploadMessage: ValueCallback<Uri>? = null
    var mUploadMessageArray: ValueCallback<Array<Uri>>? = null
    var photoPath: String? = null
    var isNeverAskCameraPermissionAgain = false
    var cameraPermissionDenied = false
    var mWebView: WebView? = null
    var qrString: String? = null
    lateinit var customWebChromeClient: CustomChromeClient
    private val REQUEST_CAMERA_PERMISSION = 1
    private val PERM_CAMERA = arrayOf(Manifest.permission.CAMERA)
    internal var mPermissionRequest: PermissionRequest? = null
    private var alertBox1: AlertDialog? = null
    private var neverAsk: AlertDialog? = null
    private val REQUEST_ID_MULTIPLE_PERMISSIONS = 15
    private var qrCodeText: String? = null
    private var qrCodeCaptureFragment: QrCodeCaptureFragment? = null
    var rlProgress: RelativeLayout? = null
    var cameraFilePath: String? = null
    var tempFile: File? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        mWebView = findViewById<WebView>(R.id.webview)
        rlProgress = findViewById<RelativeLayout>(R.id.progress_bar)
//        mWebView!!.webViewClient = object : WebViewClient() {
//            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
//                view?.loadUrl(url)
//                return true
//            }
//        }
        mWebView!!.settings.javaScriptEnabled = true
        mWebView!!.settings.loadWithOverviewMode = true
        mWebView!!.settings.useWideViewPort = true
        mWebView!!.settings.allowFileAccess = true
        mWebView!!.settings.domStorageEnabled = true
        mWebView!!.settings.javaScriptCanOpenWindowsAutomatically = true
        mWebView!!.settings.allowContentAccess = true
        mWebView!!.settings.allowFileAccessFromFileURLs = true
        mWebView!!.settings.allowUniversalAccessFromFileURLs = true
        mWebView!!.settings.setSupportMultipleWindows(true)
        mWebView!!.settings.setGeolocationEnabled(true)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true)
        }

        mWebView!!.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            val request = DownloadManager.Request(Uri.parse(url))
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                URLUtil.guessFileName(url, contentDisposition, mimetype)
            )
            val dm = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            dm.enqueue(request)
            Toast.makeText(this, "Downloading File", Toast.LENGTH_LONG).show()
        }

        mWebView!!.addJavascriptInterface(JavaScriptInterface(), "Android")

        mWebView!!.webViewClient = CustomWebViewClient()
        customWebChromeClient = CustomChromeClient()
        mWebView!!.webChromeClient = customWebChromeClient

        if (checkAndRequestPermissions())
            onAllowMandatePermission(savedInstanceState)

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (supportFragmentManager.backStackEntryCount > 0) {
                supportFragmentManager.popBackStack()
                return true
            } else if (mWebView!!.canGoBack()) {
                mWebView!!.goBack()
                return true
            } else {
                super.onBackPressed()
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mWebView?.saveState(outState)
    }

    override fun onPostResume() {
        super.onPostResume()
        mWebView?.onResume()
    }

    inner class CustomWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            if (rlProgress != null)
                rlProgress!!.visibility = View.VISIBLE
            super.onPageStarted(view, url, favicon)
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            if (rlProgress != null)
                rlProgress!!.visibility = View.GONE
            super.onPageFinished(view, url)
        }

    }

    override fun onResume() {
        super.onResume()
        supportActionBar?.hide()
    }

//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        @NonNull permissions: Array<String>,
//        @NonNull grantResults: IntArray
//    ) {
//        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
//    }

    @NeedsPermission(Manifest.permission.CAMERA)
    fun openCamera(
        webView: WebView, filePathCallback: ValueCallback<Array<Uri>>,
        fileChooserParams: WebChromeClient.FileChooserParams
    ) {
        customWebChromeClient.onShowFileChooser(webView, filePathCallback, fileChooserParams)
    }

    fun askCameraPermission(
        webView: WebView, filePathCallback: ValueCallback<Array<Uri>>,
        fileChooserParams: WebChromeClient.FileChooserParams
    ) {
//        MainActivityPermissionsDispatcher.openCameraWithCheck(this, webView, filePathCallback, fileChooserParams)
    }

//    @OnNeverAskAgain(Manifest.permission.CAMERA)
//    internal fun onPermissionNeverAskAgain() {
//        isNeverAskCameraPermissionAgain = true
//        cameraPermissionDenied = false
//        val intent = Intent(this, TransparentActivity::class.java)
//        val bundle = Bundle()
//        bundle.putBoolean("isKill", true)
//        intent.putExtras(bundle)
//        startActivityForResult(intent, PERMISSION_DENIED)
//    }


//    @OnPermissionDenied(Manifest.permission.CAMERA)
//    internal fun onCameraPermissionDenied() {
//        cameraPermissionDenied = true
//        isNeverAskCameraPermissionAgain = false
//        val intent = Intent(this, TransparentActivity::class.java)
//        val bundle = Bundle()
//        bundle.putBoolean("isKill", true)
//        intent.putExtras(bundle)
//        startActivityForResult(intent, PERMISSION_DENIED)
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (Build.VERSION.SDK_INT >= 21) {
            if (requestCode == SCAN_QR) {
                if (resultCode == Activity.RESULT_OK) {
                    //here is where you get your result
                    qrString = intent?.getStringExtra("SCAN_RESULT")
                    mWebView?.loadUrl("javascript:setMerchantQR('hello');")
                }
            } else {
                if (requestCode == PERMISSION_DENIED) {
                    mUploadMessageArray!!.onReceiveValue(null)
                    mUploadMessageArray = null
                } else {
                    if (null == mUploadMessageArray) {
                        super.onActivityResult(requestCode, resultCode, intent)
                        return
                    } else {
                        var results: Array<Uri>? = null
                        if (requestCode == FILE_CHOOSER) {
                            if (resultCode == RESULT_OK) {
                                if (intent != null) {
                                    val dataString = intent.dataString
                                    if (dataString != null) {
                                        results = arrayOf(Uri.parse(dataString))
                                    }


                                    val result = intent.data
                                    if (result == null) {
                                        mUploadMessageArray!!.onReceiveValue(null)
                                        mUploadMessageArray = null
                                        return
                                    }

                                    var fPath = PathUtils.getRealPath(this@MainActivity, result)
                                    if(fPath == null){
                                        fPath = cameraFilePath
                                    }
                                    var file : File? = null
                                    if(fPath != null) {
                                        file = File(fPath)

                                        if (!file.exists()) {
                                            mUploadMessageArray!!.onReceiveValue(null)
                                            mUploadMessageArray = null
                                            Toast.makeText(
                                                applicationContext,
                                                "Please select a local Image File.", Toast.LENGTH_LONG
                                            )
                                                .show()
                                            return
                                        }
                                    } else{
                                        Toast.makeText(
                                            applicationContext,
                                            "Cannot read the file. Please select another file", Toast.LENGTH_LONG
                                        )
                                            .show()
                                        return
                                    }

                                    //Resize and upload
                                    file = compressImageFile(file)
                                    if(file != null) {
                                        mUploadMessageArray!!.onReceiveValue(
                                            getImageContentUri(
                                                this@MainActivity,
                                                file
                                            )
                                        )
                                        mUploadMessageArray = null
                                    } else{
                                        mUploadMessageArray!!.onReceiveValue(null)
                                        mUploadMessageArray = null
                                        return
                                    }
                                    return
                                }


                            }
                            mUploadMessageArray!!.onReceiveValue(results)
                            mUploadMessageArray = null
                        } else if (requestCode == IMAGE_CAPTURE) {
                            if (resultCode == RESULT_OK) {
                                if (photoPath != null) {
                                    results = arrayOf(Uri.parse(photoPath))
                                }
                            }
                            mUploadMessageArray!!.onReceiveValue(results)
                            mUploadMessageArray = null
                        } else {
                            super.onActivityResult(requestCode, resultCode, intent)
                            return
                        }
                    }
                }
            }
        } else {
            if (requestCode == SCAN_QR) {
                if (resultCode == Activity.RESULT_OK) {
                    //here is where you get your result
                    qrString = intent?.getStringExtra("SCAN_RESULT")
                }
            } else {
                if (null == mUploadMessageArray) {
                    super.onActivityResult(requestCode, resultCode, intent)
                    return
                } else {
                    if (requestCode == IMAGE_CAPTURE || requestCode == FILE_CHOOSER) {
                        if (null == mUploadMessage) return
                        val result = if (intent == null || resultCode != RESULT_OK) null else intent.data
                        mUploadMessage!!.onReceiveValue(result)
                        mUploadMessage = null
                    } else {
                        super.onActivityResult(requestCode, resultCode, intent)
                        return
                    }
                }
            }
        }
    }

    fun getImageContentUri(context: Context, imageFile: File): Array<Uri>? {
        val filePath = imageFile.absolutePath
        val cursor = context.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            arrayOf(MediaStore.Images.Media._ID),
            MediaStore.Images.Media.DATA + "=? ",
            arrayOf(filePath), null
        )

        if (cursor != null && cursor.moveToFirst()) {
            val id = cursor.getInt(
                cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID)
            )
            val baseUri = Uri.parse("content://media/external/images/media")
            return arrayOf(Uri.withAppendedPath(baseUri, "" + id))
        } else {
            return if (imageFile.exists()) {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.DATA, filePath)
                arrayOf(
                    context.contentResolver.insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values
                    )
                )
            } else {
                null
            }
        }

    }


    private fun callJavaScriptFunction(webView: WebView, script: String) {
        webView.post {
            run {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    webView.evaluateJavascript(script, ValueCallback {

                    })
                } else {
                    webView.loadUrl("javascript:$script")
                }
            }
        }
    }

    fun formatScript(
        function: String,
        @Nullable vararg params: Any
    ): String {

        val builder = StringBuilder(function).append('(')
        val length = params.size
        for (i in params.indices) {
            if (params[i] is String) {
                builder.append("\'")
            }
            builder.append(params[i])
            if (params[i] is String) {
                builder.append("\'")
            }
            if (i != length - 1) {
                builder.append(",")
            }
        }

        builder.append(')')
        return builder.toString()
    }

    inner class CustomChromeClient : WebChromeClient() {

        override fun onCreateWindow(
            view: WebView?,
            isDialog: Boolean,
            isUserGesture: Boolean,
            resultMsg: Message?
        ): Boolean {

            val newWebView = WebView(this@MainActivity)
            view?.addView(newWebView)
            val transport = resultMsg?.obj as WebView.WebViewTransport
            transport.webView = newWebView
            resultMsg.sendToTarget()

            newWebView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    val browserIntent = Intent(Intent.ACTION_VIEW)
                    browserIntent.data = Uri.parse(url)
                    startActivity(browserIntent)
                    return true
                }
            }
            return true
        }


        //For Android 3.0+
        fun openFileChooser(uploadMsg: ValueCallback<Uri>) {
            mUploadMessage = uploadMsg
            val i = Intent(Intent.ACTION_GET_CONTENT)
            i.addCategory(Intent.CATEGORY_OPENABLE)
            i.type = "*/*"
            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILE_CHOOSER)
        }

        //For Android 4.1+
        fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String, capture: String) {
            mUploadMessage = uploadMsg
            val i = Intent(Intent.ACTION_GET_CONTENT)
            i.addCategory(Intent.CATEGORY_OPENABLE)
            i.type = "*/*"
            startActivityForResult(Intent.createChooser(i, "File Chooser"), FILE_CHOOSER)
        }

        //For Android 5.0+
        override fun onShowFileChooser(
            webView: WebView, filePathCallback: ValueCallback<Array<Uri>>,
            fileChooserParams: WebChromeClient.FileChooserParams
        ): Boolean {
            if (fileChooserParams.isCaptureEnabled && ActivityCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_DENIED
            ) {
                if (!isNeverAskCameraPermissionAgain) {
                    askCameraPermission(webView, filePathCallback, fileChooserParams)
                    return true
                } else {
                    isNeverAskCameraPermissionAgain = true
                    askCameraPermission(webView, filePathCallback, fileChooserParams)
                    return true
                }
            } else {
                if (mUploadMessageArray != null) {
                    mUploadMessageArray!!.onReceiveValue(null)
                }
                mUploadMessageArray = filePathCallback
                var takePictureIntent: Intent? = null
                var contentSelectionIntent: Intent? = null
                if (fileChooserParams.isCaptureEnabled) {
                    takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    if (takePictureIntent.resolveActivity(packageManager) != null) {
                        var photoFile: File? = null
                        try {
                            photoFile = createImageFile()
//                            photoFile = compressImageFile(photoFile)
                            takePictureIntent.putExtra("PhotoPath", photoPath)
                        } catch (ex: IOException) {
                            Log.e("WebViewFragment", "Image file creation failed", ex)
                        }

                        if (photoFile != null) {

                            // Fix for
                            // file:// is not allowed to attach with Intent anymore or it will throw FileUriExposedException which may cause your app crash immediately called
                            val builder = StrictMode.VmPolicy.Builder()
                            StrictMode.setVmPolicy(builder.build())

                            photoPath = "file:" + photoFile.absolutePath
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile))
                        } else {
//                            takePictureIntent = null
                        }
                        if (takePictureIntent != null) {
                            takePictureIntent.putExtra(Intent.EXTRA_TITLE, "Capture Image")
                            startActivityForResult(takePictureIntent, IMAGE_CAPTURE)
                        }
                    }
                } else {
                    contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                    contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                    contentSelectionIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
                    val mimeTypes = arrayOf("image/jpeg", "image/png")
                    contentSelectionIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
                    contentSelectionIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this@MainActivity, BuildConfig.APPLICATION_ID + ".provider", createImageFileUri()))

                    contentSelectionIntent.type = "image/*"
                    startActivityForResult(contentSelectionIntent, FILE_CHOOSER)
                }
                return true
            }
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onPermissionRequest(request: PermissionRequest) {
            mPermissionRequest = request
            val requestedResources = request.resources
            for (r in requestedResources) {
                if (r == PermissionRequest.RESOURCE_VIDEO_CAPTURE) {
                    // accept camera capture request.
                    val alertDialogBuilder = AlertDialog.Builder(this@MainActivity)
                        .setTitle("Allow Camera Permission")
                        .setPositiveButton("Allow") { dialog, which ->
                            dialog.dismiss()
                            mPermissionRequest!!.grant(arrayOf(PermissionRequest.RESOURCE_VIDEO_CAPTURE))
                        }
                        .setNegativeButton("Deny") { dialog, which ->
                            dialog.dismiss()
                            mPermissionRequest!!.deny()
                        }
                    val alertDialog = alertDialogBuilder.create()
                    alertDialog.show()

                    break
                }
            }
        }

        override fun onGeolocationPermissionsShowPrompt(origin: String?, callback: GeolocationPermissions.Callback?) {
            callback!!.invoke(origin, true, false)
        }

        override fun onPermissionRequestCanceled(request: PermissionRequest) {
            super.onPermissionRequestCanceled(request)
            Toast.makeText(this@MainActivity, "Permission Denied", Toast.LENGTH_SHORT).show()
        }

    }

    // Create an image file
    @Throws(IOException::class)
    fun createImageFile(): File {
        @SuppressLint("SimpleDateFormat") val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "img_" + timeStamp + "_"
        val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, ".jpg", storageDir)
    }

    @Throws(IOException::class)
    fun createImageFileUri(): File{
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        //This is the directory in which the file will be created. This is the default location of Camera photos
        val storageDir = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera")
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        )
        // Save a file: path for using again
        cameraFilePath = image.absolutePath
        tempFile = image
        return image
    }

    private inner class JavaScriptInterface {
        @JavascriptInterface
        fun closeMyActivity() {
            finish()
        }

        @JavascriptInterface
        fun scanBarcode(param: String?) {
            performScan()
        }

        @JavascriptInterface
        fun shareQR(qrString: String?) {
            if (qrString!!.isNotEmpty()) {
                val qrData = Gson().fromJson<QrData>(qrString, QrData::class.java)
                if (qrData?.qr != null && qrData.qr.isNotEmpty()) {
                    val intent = Intent(this@MainActivity, QrCodeCaptureActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString("qrString", qrData.qr)
                    intent.putExtras(bundle)
                    startActivity(intent)
                } else {
                    Toast.makeText(this@MainActivity, "Qr Code Not found", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this@MainActivity, "Qr Code Not found", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if (checkAndRequestPermissions()) {
            onAllowMandatePermission(savedInstanceState)
        }
    }

    private fun onAllowMandatePermission(savedInstanceState: Bundle?) {

        if (savedInstanceState != null)
            mWebView!!.restoreState(savedInstanceState)
        else
            mWebView!!.loadUrl("https://merchant.freecharge.in/?fcChannel=3")

    }

    private fun onAllowMandatePermission() {

        mWebView!!.loadUrl("https://merchant.freecharge.in/?fcChannel=3")
    }

    @OnNeverAskAgain(
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun NeverAskMandate() {
        if (neverAsk == null) {
            neverAsk = AlertDialog.Builder(this)
                .setPositiveButton("Grant", DialogInterface.OnClickListener { dialog, which ->
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    intent.addCategory(Intent.CATEGORY_DEFAULT)
                    intent.data = Uri.parse("package:$packageName")
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                    startActivity(intent)
                    Toast.makeText(applicationContext, "Please grant the required permissions", Toast.LENGTH_LONG)
                        .show()
                    dialog.dismiss()
                })
                .setNegativeButton(R.string.button_deny, DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                    Toast.makeText(
                        this@MainActivity,
                        "FreeCharge Merchant won't work without Camera, Location and storage permission",
                        Toast.LENGTH_LONG
                    ).show()
                    finish()
                })
                .setCancelable(false)
                .setTitle("Permission Denied")
                .setMessage("FreeCharge Merchant won't work without Camera, Location and storage permission. Tap on GRANT to give this permission to FreeCharge Merchant.")
                .show()
        } else if (!neverAsk!!.isShowing) {
            neverAsk!!.show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {
                val perms = HashMap<String, Int>()
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
//                perms[Manifest.permission.RECEIVE_SMS] = PackageManager.PERMISSION_GRANTED
//                perms[Manifest.permission.READ_PHONE_STATE] = PackageManager.PERMISSION_GRANTED
                if (grantResults.size > 0) {
                    for (i in permissions.indices) {
                        perms[permissions[i]] = grantResults[i]
                    }
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED &&
                        perms[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED &&
                        perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED &&
                        perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
//                        && perms[Manifest.permission.READ_SMS] == PackageManager.PERMISSION_GRANTED
//                        && perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
//                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    ) {
                        onAllowMandatePermission()
                    } else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                            || ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            )
                            || ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                            ) || ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            )
//                            || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_SMS)
//                            || ActivityCompat.shouldShowRequestPermissionRationale(
//                                this,
//                                Manifest.permission.WRITE_EXTERNAL_STORAGE
//                            )
                        ) {
                            if (alertBox1 == null) {
                                alertBox1 = AlertDialog.Builder(this)
                                    .setPositiveButton("Grant", DialogInterface.OnClickListener { dialog, which ->
                                        dialog.dismiss()
                                        checkAndRequestPermissions()
                                    })
                                    .setNegativeButton(
                                        R.string.button_deny,
                                        DialogInterface.OnClickListener { dialog, which ->
                                            dialog.dismiss()
                                            finish()
                                        })
                                    .setCancelable(false)
                                    .setTitle("Permission Denied")
                                    .setMessage("FreeCharge Merchant won't work without Camera, Location and storage permission. Tap on GRANT to give this permission to FreeCharge Merchant.")
                                    .show()
                            } else if (!alertBox1!!.isShowing) {
                                alertBox1!!.show()
                            }
                        } else {
                            NeverAskMandate()
                        }
                    }
                }
            }
        }
    }

    private fun checkAndRequestPermissions(): Boolean {
        if (alertBox1 != null && alertBox1!!.isShowing)
            return false
        val permissionReadMessage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
        val permissionReadExternalStorage =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val permissionCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val permissionExternalStorage =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val listPermissionsNeeded = ArrayList<String>()

        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (permissionExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (permissionReadExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                REQUEST_ID_MULTIPLE_PERMISSIONS
            )
            return false
        }
        return true
    }

    //region qr scanner callbacks
    override fun onCodeCaptured(barcode: String, fragment: QrCodeCaptureFragment) {
        qrCodeCaptureFragment = fragment
        try {
            removeFragment(qrCodeCaptureFragment!!)
            onQrCodeCaptured(barcode)
        } catch (e: WriterException) {
            e.printStackTrace()
        }

    }

    override fun onError(t: QrScannerException) {
        if (t.errorCode == QrScannerException.ERROR_GALLERY_WRONG_IMAGE) {
            Toast.makeText(this, "Error reading QR. Please select another image", Toast.LENGTH_LONG).show()
        }
    }

    override fun removeFragment(thisFragment: QrCodeCaptureFragment) {
        thisFragment.removeReceiver(this)
        supportFragmentManager.beginTransaction().remove(thisFragment).commit()
        qrCodeCaptureFragment = null
    }

    fun performScan() {
        qrCodeCaptureFragment = QrCodeCaptureFragment.newInstance(this@MainActivity, true, false, true, this)
        supportFragmentManager.beginTransaction().replace(
            R.id.temp_layout, qrCodeCaptureFragment,
            qrCodeCaptureFragment!!.tag
        ).addToBackStack(qrCodeCaptureFragment!!.tag).commitAllowingStateLoss()
    }

    @Throws(WriterException::class)
    private fun onQrCodeCaptured(@NonNull qrCodeText: String) {
        this.qrCodeText = qrCodeText
        val script = formatScript(
            "window.setMerchantQR", qrCodeText
        )
        callJavaScriptFunction(mWebView!!, script)
    }
    //endregion

    fun compressImageFile(photoFile: File): File? {


        var compressedFile: File ?= null

        if (photoFile == null) {
            Toast.makeText(this, "Please choose an image", Toast.LENGTH_LONG).show()
        } else {

            val options = BitmapFactory.Options()
            options.inPreferredConfig = Bitmap.Config.ARGB_8888
            val bitmap = BitmapFactory.decodeFile(photoFile.absolutePath, options)

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val imageFileName = "FcMerchant_" + timeStamp + getFileExtension(photoFile)
            if(bitmap != null) {
                if (!(bitmap.width == 0 && bitmap.height == 0)) {
                    // Compress image in main thread using custom Compressor
                    try {
                        compressedFile = Compressor(this)
                            .setMaxWidth(1024)
                            .setMaxHeight(768)
                            .setQuality(75)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG)

                            .setDestinationDirectoryPath(
                                Environment.getExternalStoragePublicDirectory(

                                    Environment.DIRECTORY_PICTURES + "/fcMerchant"
                                ).absolutePath
                            )
                            .compressToFile(photoFile, imageFileName)
                    } catch (e: Exception){
                        return compressedFile
                    }
                }
            } else{
                Toast.makeText(this, "Not a valid image. Please select another image.", Toast.LENGTH_LONG).show()
            }
            return compressedFile
        }

        return compressedFile
    }

    private fun getFileExtension(file: File): String {
        val name = file.name
        val lastIndexOf = name.lastIndexOf(".")
        return if (lastIndexOf == -1) {
            "" // empty extension
        } else name.substring(lastIndexOf)
    }
}
